# docker环境部署

## docker环境安装
### docker安装
1. 安装yum-utils：
yum install -y yum-utils \
device-mapper-persistent-data \
lvm2
2. 为yum源添加docker仓库位置：
yum-config-manager \
--add-repo \
https://download.docker.com/linux/centos/docker-ce.repo
3. 安装docker:
yum install docker-ce
4. 启动docker:
systemctl start docker
注：常见命令见document/reference文件夹中的docker.md
5. 安装上传下载插件：
yum -y install lrzsz
### docker compose安装
1. 下载地址：https://github.com/docker/compose/releases
2. 安装地址：/usr/local/bin/docker-compose
3. 设置为可执行：sudo chmod +x /usr/local/bin/docker-compose
4. 测试是否安装成功：docker-compose --version

## 为实现应用自动化部署，需要你在客户端也安装docker环境，例如安装在windows机器
 windows下安装docker可以自行百度安装，安装完成配置一下dcoker文件,即加入Harbor访问地址
![image-20200130133118860](./img/respos.png)

### 登录

```
docker login 192.168.43.68  (harbor默认用户名密码admin/Harbor12345)
```

