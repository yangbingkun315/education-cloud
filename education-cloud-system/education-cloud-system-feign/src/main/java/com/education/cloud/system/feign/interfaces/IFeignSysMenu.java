package com.education.cloud.system.feign.interfaces;

import com.education.cloud.system.feign.qo.SysMenuQO;
import com.education.cloud.system.feign.vo.SysMenuVO;
import com.education.cloud.util.base.Page;
import com.education.cloud.util.constant.ServiceConstant;
import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * 菜单信息
 *
 * @author wujing
 */
@FeignClient(name = ServiceConstant.SYSTEM_SERVICE,contextId = "sysMenuClient")
public interface IFeignSysMenu {


    @RequestMapping(value = "/feign/system/sysMenu/listForPage")
    Page<SysMenuVO> listForPage(@RequestBody SysMenuQO qo);

    @RequestMapping(value = "/feign/system/sysMenu/save")
    int save(@RequestBody SysMenuQO qo);

    @RequestMapping(value = "/feign/system/sysMenu/deleteById")
    int deleteById(@RequestBody Long id);

    @RequestMapping(value = "/feign/system/sysMenu/updateById")
    int updateById(@RequestBody SysMenuQO qo);

    @RequestMapping(value = "/feign/system/sysMenu/getById")
    SysMenuVO getById(@RequestBody Long id);

    @RequestMapping(value = "/feign/system/sysMenu/listByUserAndMenu")
    List<String> listByUserAndMenu(@RequestBody Long userNo);

}
