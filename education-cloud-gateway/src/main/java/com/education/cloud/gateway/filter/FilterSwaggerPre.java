/**
 * Copyright 2015-现在 广州市领课网络科技有限公司
 */
package com.education.cloud.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;
import springfox.documentation.swagger2.web.Swagger2Controller;


/**
 * @Description 适配后端微服务swagger接口，方便网关进行文档聚合
 * @Date 2020/3/23
 * @Created by 67068
 */

@Component
public class FilterSwaggerPre extends ZuulFilter {

    private static final Logger logger = LoggerFactory.getLogger(FilterSwaggerPre.class);


    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }


    /**
     * 数字越小，优先级越高
     * 一定要在 {@link org.springframework.cloud.netflix.zuul.filters.pre.PreDecorationFilter} 过滤器之后执行，因为这个过滤器做了路由
     */
    @Override
    public int filterOrder() {
        return FilterConstants.PRE_DECORATION_FILTER_ORDER + 1;
    }

    @Override
    public boolean shouldFilter() {
        String uri = RequestContext.getCurrentContext().getRequest().getServletPath();
        logger.info("请求地址", uri);

        if ( uri.indexOf(Swagger2Controller.DEFAULT_URL)>0 ) {
            RequestContext.getCurrentContext().set("requestURI","/v2/api-docs");
        }

        return false;
    }

    @Override
    public Object run()  {
        return null;
    }


}
